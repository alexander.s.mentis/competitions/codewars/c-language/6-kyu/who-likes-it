#include <stdio.h>
#include <stdlib.h>
#include <string.h>

// result string must be a heap-allocated, nul-terminated string
// to be freed by the tests suite
char *likes(size_t n, const char *const names[n]) {
    //  <----  hajime!
  
    char buffer[512];
  
    switch(n) {
        case 0 : {
          sprintf(buffer, "no one likes this");
          break;
        }
        case 1 : {
          sprintf(buffer, "%s likes this", names[0]);
          break;
        }
        case 2 : {
          sprintf(buffer, "%s and %s like this", names[0], names[1]);
          break;
        }
        case 3 : {
          sprintf(buffer, "%s, %s and %s like this", names[0], names[1], names[2]);
          break;
        }
        default : {
          sprintf(buffer, "%s, %s and %d others like this", names[0], names[1], n-2);
        }
    }
   
    char *result = calloc(strlen(buffer)+1, sizeof(char));
    memcpy(result, buffer, strlen(buffer)+1);
  
	  return result;
}